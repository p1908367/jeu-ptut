package com;

import java.awt.Graphics;
import java.awt.Rectangle;

public class Camera extends GameObject{

	private Handler handler;
	int x;
	private int y;
	
	public Camera(int x, int y,ID id) {
		super(x, y, id, 0, 0);
		this.x = x;
		this.y = y;	
		id = this.id;
	}
	
	
	public void tick(GameObject player) {
		//this.x = handler.object.get(0).getX();
		//this.x = -player.getX() + 640/2;	
		//y = handler.object.get(0).getY()/2;
	}

	
	public int getX() {
		return (int) x;
	}

	public void setX(int x) {
		this.x = x;
		if(this.x<0)
			this.x=0;
		if(this.x+Main.WIDTH>Main.mapSizeX)
			this.x=Main.mapSizeX-Main.WIDTH;
	}

	public int getY() {
		return (int) y;
	}

	public void setY(int y) {
		this.y = y;
		if(this.y<0)
			this.y=0;
		if(this.y+Main.HEIGHT>Main.mapSizeY)
			this.y=Main.mapSizeY-Main.HEIGHT;
	}

	@Override
	public Rectangle getBounds() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void tick() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(Graphics g) {
		// TODO Auto-generated method stub
		
	}
	
	
}