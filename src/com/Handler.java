package com;

import java.awt.*;
import java.util.LinkedList;

public class Handler {
    LinkedList<GameObject> object= new LinkedList<GameObject>();//The player must be at first position
    public Main game;

    public Handler(Main game){
        this.game=game;
    }

    public void tick(){
        for (int i=0; i < object.size()/*nombre d'object sur l'écran*/; i++){
            GameObject tempObject = object.get(i);
            tempObject.tick();
        }
    }

    public void render(Graphics g){
        for (int i=0; i < object.size()/*nombre d'object sur l'écran*/; i++){
            GameObject tempObject = object.get(i);
            tempObject.render(g);
        }
    }
    public void addObject (GameObject object){
        this.object.add(object);
    }
    public void deleteobject (GameObject object){
        this.object.add(object);
    }
    /*public GameObject getObject(int id) {
    	if (id<0 || id>=object.size())
    		return null;
    	else
    		return object[id];
    	
    }*/
}
