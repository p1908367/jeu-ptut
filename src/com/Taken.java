package com;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
public class Taken extends JPanel{	//taquin pas taken...
    private int size;				// Taille du taquin
    private int nbTiles;	// Nombre de cases
    private int dimension;
    private static final Color FOREGROUND_COLOR = new Color(100, 96, 96); //Couleur des cases
    private static final Random RANDOM = new Random();	//Mélange des cases
    private int[] tiles;	// Chaîne on stoque les cases
    private int tileSize;	// Taille des cases
    private int blankPos;	// Position de la case blanche
    private int margin;	//Marge entre les case et la fenêtre
    private int gridSize;	// Taille de la grille
    private boolean gameOver; // Indique l'état du jeu du taquin
    static int window_State = 1;

    public Taken(int size, int dim, int mar) {
        this.size = size;
        dimension = dim;
        margin = mar;

        nbTiles = size * size - 1; // -1 pcq on ne compte pas la case vide
        tiles = new int[size * size];

        gridSize = (dim - 2 * margin);
        tileSize = gridSize / size;

        setPreferredSize(new Dimension(dimension, dimension + margin));
        setBackground(Color.WHITE);
        setForeground(FOREGROUND_COLOR);
        setFont(new Font("SansSerif", Font.BOLD, 60));

        gameOver = true;

        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {	// Pour récupérer les actions de la souris
                if (gameOver) {
                	newGame();
                } else {
                    // Récupère la position du clique
                    int ex = e.getX() - margin;
                    int ey = e.getY() - margin;

                    if (ex < 0 || ex > gridSize  || ey < 0  || ey > gridSize)  // on vérifie que le cliqe soit dans la grille
                        return;

                    // On regarde où est le clique dans la grille
                    int c1 = ex / tileSize;
                    int r1 = ey / tileSize;

                    // On récupère la position de la case blanche
                    int c2 = blankPos % size;
                    int r2 = blankPos / size;


                    int clickPos = r1 * size + c1; // On convertie la position en coordonné unidimensionnel

                    int dir = 0;


                    if (c1 == c2  &&  Math.abs(r1 - r2) > 0)
                        dir = (r1 - r2) > 0 ? size : -size;
                    else if (r1 == r2 && Math.abs(c1 - c2) > 0)
                        dir = (c1 - c2) > 0 ? 1 : -1;

                    if (dir != 0) {
                        do {		//On déplace les cases
                            int newBlankPos = blankPos + dir;
                            tiles[blankPos] = tiles[newBlankPos];
                            blankPos = newBlankPos;
                        } while(blankPos != clickPos);

                        tiles[blankPos] = 0;
                    }

                    gameOver = isSolved();	//On vérifie si le jeu est résolu
                }

                repaint();   //On repaint la grille
            }
        });

        newGame();
    }

    private void newGame() {  // Le contructeur pour faire une partie
        do {
            reset();
            shuffle();
        } while(!isSolvable());

        gameOver = false;
    }

    private void reset() {		// La méthode pour réinitialiser
        for (int i = 0; i < tiles.length; i++) {
            tiles[i] = (i + 1) % tiles.length;
        }

        blankPos = tiles.length - 1;
    }

    private void shuffle() {		//Méthode pour mélanger les cases
        int n = nbTiles;

        while (n > 1) {
            int r = RANDOM.nextInt(n--);
            int tmp = tiles[r];
            tiles[r] = tiles[n];
            tiles[n] = tmp;
        }
    }


    private boolean isSolvable() {  //Méthode pour vérifier si la grille peut être résolu
        int countInversions = 0;

        for (int i = 0; i < nbTiles; i++) {
            for (int j = 0; j < i; j++) {
                if (tiles[j] > tiles[i])
                    countInversions++;
            }
        }

        return countInversions % 2 == 0;
    }

    private boolean isSolved() {	//Méthode pour vérifier si la grille est résolu
        if (tiles[tiles.length - 1] != 0)
            return false;

        for (int i = nbTiles - 1; i >= 0; i--) {
            if (tiles[i] != i + 1)
                return false;
        }

        return true;
    }

    private void drawGrid(Graphics2D g) {
        for (int i = 0; i < tiles.length; i++) {
            int r = i / size;
            int c = i % size;
            int x = margin + c * tileSize;
            int y = margin + r * tileSize;

            if(tiles[i] == 0) {
                if (gameOver) {
                    g.setColor(FOREGROUND_COLOR);
                    drawCenteredString(g, "\u2713", x, y);
                }

                continue;
            }

            // Pour les autres case
            g.setColor(getForeground());
            g.fillRoundRect(x, y, tileSize, tileSize, 25, 25);
            g.setColor(Color.BLACK);
            g.drawRoundRect(x, y, tileSize, tileSize, 25, 25);
            g.setColor(Color.WHITE);

            drawCenteredString(g, String.valueOf(tiles[i]), x , y);
        }
    }

    private void drawStartMessage(Graphics2D g) {
        if (gameOver) {
        	Main.experience += 100;
        	Main.argent += 20;
            g.setFont(getFont().deriveFont(Font.BOLD, 18));
            g.setColor(FOREGROUND_COLOR);
            String s = "Bravo vous avez re�u 100 xp et 20�";
            g.drawString(s, (getWidth() - g.getFontMetrics().stringWidth(s)) / 2,
                    getHeight() - margin);
        }
    }

    private void drawCenteredString(Graphics2D g, String s, int x, int y) {
        FontMetrics fm = g.getFontMetrics();
        int asc = fm.getAscent();
        int desc = fm.getDescent();
        g.drawString(s,  x + (tileSize - fm.stringWidth(s)) / 2,
                y + (asc + (tileSize - (asc + desc)) / 2));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2D = (Graphics2D) g;
        g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        drawGrid(g2D);
        drawStartMessage(g2D);
    }


       public static void paint(){
            JFrame frame = new JFrame();
            frame.setDefaultCloseOperation(window_State = 0);
            frame.setTitle("Hack Taquin");
            frame.setResizable(false);
            frame.add(new Taken(3, 500, 30), BorderLayout.CENTER);			//On crée une partie
            frame.pack();
            // center on the screen
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);
        }

}

