package com;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.GdxRuntimeException;


public class Main extends Canvas implements Runnable {
    
	private static final long serialVersionUID = 1L;
	public static final int WIDTH=640;//screen size
    public static final int HEIGHT=WIDTH/12*9;
    public static int mapSizeX=900;//map size
    public static int mapSizeY=700;
    public static boolean camState = false;
    private  Thread thread;
    private boolean running=false; // pour voir si le jeu est ouvert ou pas
    private Handler handler;
    private Menu menu;
    private static int actualMapID=0;
    public Camera camera;
    public String[] arguments;
    public boolean erreur=false;
    public boolean launched=false;
    public static int argent = 0;
    public static int niveau = 0;
    public static int experience = 20;
    public int nivSup=150;
    private BufferedImage test;
    private Display display;
    private Table root;
    private DialogueBox dialogueBox;
    private Skin skin;
    private SpriteBatch sb;
    private BufferedImage image = new BufferedImage(WIDTH,HEIGHT,BufferedImage.TYPE_INT_RGB);
    private BufferedImage spriteSheet=null;
    private Player p;
   private String title;
   private Window window;
   private BufferImageLoader load;
    public void init() {
        BufferImageLoader loader = new BufferImageLoader();
        try {
            spriteSheet =loader.loadImage("/res/personne.png");
        }catch (IOException e ){
            e.printStackTrace();
        }
        
    }
   
    
    public void initIU() {
    	BufferImageLoader load = new BufferImageLoader();
    	
    	try {
			test= load.loadImage("/res/dialoguebox.png");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
    }
    


	



	public enum STATE{
            menu,
            game
    };
    public STATE gameState= STATE.menu;

    public Main(String[] args) {
    	this.arguments=args;
        handler = new Handler(this);
        menu = new Menu(this,handler);
        this.addKeyListener(new KeyInput(handler, this));
        this.addMouseListener(menu);
        camera = new Camera(0,0, ID.Player);
        //camera.x = handler.object.get(0).getX();
        new Window(WIDTH, HEIGHT, "THE HACKER", this);
        


    }


    public synchronized void start(){
        thread=new Thread(this);
        thread.start();
        running=true;
    }

    public synchronized void stop(){
        try {// try catch c'est comme if else		Pardon!? qui a dit cette connerie??
            thread.join();
            running=false;
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void run() { //La méme game loop de Minecraft
    	init();
    	initIU();
        this.requestFocus();
        long lastTime = System.nanoTime();
        double amountOfTicks = 60.0;
        double ns = 1000000000 / amountOfTicks;
        double delta = 0;
        long timer = System.currentTimeMillis();
        int frames = 0;
        while(running) {
            long now = System.nanoTime();
            delta += (now - lastTime) / ns;
            lastTime = now;
            while(delta >= 1) {
                tick();
                delta--;
            }
            if (running)
                render();
            frames++;

            if(System.currentTimeMillis() - timer > 1000) {
                timer += 1000;
                frames = 0;
                //updates = 0;
            }
            if(erreur)
            	running=false;
            
            if(experience >= nivSup) {
            	experience = experience-nivSup;
            	niveau += 1;
            	nivSup+=niveau*30; //provisoire, pour voir l'augmentation de l'xp requis pour monter de niveau
            	};   // � modifier plus tard
        }
        stop();
    }

    private void tick(){
        handler.tick();
        if (gameState==STATE.game){
        	//System.out.println("x = "+ handler.object.get(0).getX() + " y = "+ handler.object.get(0).getY());       Affiche la position
        	if (camState == true) {
        		//System.out.println(handler.object.get(0).getX()- WIDTH/2 -25);
        		Player player=(Player)handler.object.get(0);
        		camera.setX((player.getX()) - (WIDTH/2) +(player.w/2) + player.offsetCameraX);
            	camera.setY((player.getY()) - (HEIGHT/2) +(player.h/2) + player.offsetCameraY);
            	//System.out.println(handler.object.get(0).getX());
                  
            	//System.out.println(camera.getX());
            	/*if (handler.object.get(0).vitesseY == 0) {
            		camera.setY((handler.object.get(0).getX()) - WIDTH/2 +25 + Player.getOffsetCameraY() );
            	}            	
            	if (handler.object.get(0).vitesseX == 0 || handler.object.get(0).vitesseX == 0) {
            		camera.setX(handler.object.get(0).getY() - HEIGHT/2 +30 + Player.getOffsetCameraX() );}*/
        	}
        	
        			
        	
            // update
        }else if(gameState==STATE.menu) {
            menu.tick();
           
        for (int i = 0; i < handler.object.size(); i++) {
        	if (handler.object.get(i).getId() == ID.Player) {
        		camera.tick(handler.object.get(i));
        		
        	}
        }
      }
    }
    
    
    private void render(){
    	
        BufferStrategy bs=this.getBufferStrategy();
        if(bs == null){
            this.createBufferStrategy(3);
            return;
        }

        Graphics g=bs.getDrawGraphics();
        g.setColor(Color.white);
        g.fillRect(0,0,WIDTH,HEIGHT);
        
        Graphics2D g2d = (Graphics2D) g;
        
        g2d.translate(-camera.getX(),-camera.getY());
        
        handler.render(g);
        g.drawImage(test,500,200,null);
        
        g2d.translate(-camera.getX(),-camera.getY());
     
        if (gameState==STATE.game){
        	/*g.setColor(Color.BLUE);
        	g.drawRect(camera.getX(), camera.getY(), 35, 35);*/
        	
        	int x=camera.getX()*2;
        	int y=camera.getY()*2;

	    	g.setColor(Color.black);
	    	
	    	g.drawString("Argent : " + argent+"�",x+20, y+50);
	    	g.drawString("Niveau : " + niveau, x+250, y+50);
	    	g.drawString("Experience : ", x+450, y+40);
	    	
	    	int xpBarSize=150;
	    	g.setColor(Color.black);
        	g.fillRect(x+450, y+50, xpBarSize, 20);
	    	g.setColor(Color.cyan);
	    	float actualXpBar=(float)experience/nivSup;
        	g.fillRect(x+450, y+50, (int) (actualXpBar * xpBarSize), 20);
        	/*g.fillRect(camera.getX()+300, camera.getY()+200, 52, 52);
        	g.setColor( Color.yellow);
        	g.fillRect(camera.getX()+500, camera.getY()+200, 52, 52);*/
        	
        	
        	
        	
        }else if(gameState==STATE.menu) {
        menu.render(g);

    }
        g.dispose();
        bs.show();
    }
    
    public static int clamp(int var,int min , int max){
        if(var>= max)
            return var=max;
            else if (var <= min)
                return var=min;
            else
                return var;
    }
    
  
    
    
    
    public static void main(String[] args) {
        new Main(args);
        
    }

    public BufferedImage getSpriteSheet() {
    	return spriteSheet;
    }
    
   
    
    public void loadMap(int id) /*throws Exception*/ {
		//handler=new Handler(this);
        File map = new File("files/maps/map"+id+".txt");
        //File map = new File("map1.txt");
    	//System.out.println(arguments[0]+"map1");
		//File map = new File(arguments[0]+"map1");
        //File map = new File(arguments[0]+"map"+id+".txt");
        String mapInfo="";
        if (map.isFile() && map.canRead() ) {
        	System.out.println("la map est utilisable");
        	try {
        	FileReader rd = new FileReader(map);
    		int ch;
    		while((ch=rd.read())!=-1) {
    			mapInfo+=(char)ch;
    		}
    		rd.close();
    		mapInfo=mapInfo.trim();//deletes the useless spaces at the beginning and the end of the string
    		//System.out.println(mapInfo);
        	}
        	catch(Exception probleme) {
        		System.out.println("Une erreur est survenue lors du chargement de la map");
        		erreur=true;
        	}
        }
        else {
        	System.out.println("erreur, la map n'est pas utilisable ou est introuvable");
        	erreur=true;
        }
    	handler.object.clear();
        handler.addObject(new Player( 10, 10, ID.Player, 32, 32, handler, this));
        handler.addObject(new Block(300, 200, ID.Talbe, 32, 32));
        handler.addObject(new Block(500,200, ID.Tableau, 32, 32));
        
        
    	//if (id==1) {
        //handler.addObject(new Player(100, 100, ID.Player, handler));
    	/*if (id==1) {
            handler.addObject(new Block(Main.WIDTH / 2, Main.HEIGHT / 2, ID.Enemy));
            handler.addObject(new Block(100, 55, ID.Enemy));
            handler.addObject(new Block(300, 160, ID.Talbe));
    	}
    	else {
            handler.addObject(new Block(200, 200, ID.Talbe));
    	}*/
        createMap(mapInfo);
    }
    
    public void createMap(String mapinfos) {
    	System.out.println("infos de la map:\n"+mapinfos+"\n\n");
    	String actualLine="";//will contain each line of the map (each line of the file)
    	int actualChar=0, /*lineStart=0,*/ len=mapinfos.length();//contains the index of the start of the line and the actual character
    	boolean multiLine=false;
    	while (actualChar<len) {
    		if(mapinfos.charAt(actualChar)=='\n' || actualChar==len-1) {//end of line
    			if(actualChar==len-1)
    				actualLine+=mapinfos.charAt(actualChar);
    			System.out.println(actualLine);
    			multiLine=lineInterpreter(actualLine);
    			if(multiLine) {
    				actualChar+=1;
    				while(actualChar<len && mapinfos.charAt(actualChar)!='}') {
    					actualLine+=mapinfos.charAt(actualChar);
    					actualChar+=1;
    				}
    				multiLineInterpreter(actualLine);
    				multiLine=false;
    			}
    			actualLine="";
    		}
    		/*else if(actualChar==len-1) {
    			actualLine+=mapinfos.charAt(actualChar);
    			System.out.println(actualLine);
    			multiLine=lineInterpreter(actualLine);
    			actualLine="";
    		}*/
    		else
    			actualLine+=mapinfos.charAt(actualChar);
    		actualChar+=1;
    	}
    	
    }
    
    public boolean lineInterpreter(String line) {/**interprets the line, creating an element of the map.
    * if the line is misspelled or unknown, does nothing
    * 
    * Rules: -example: ElementName(parameter1; parameter 2, ...)
    * 	-parameters are split by a ';'
    * 	-spaces in parameters that are not strings are ignored
    * 	-a line MUST be finished by a ')' to work properly! the line wont be used otherwise. (everything after the ')' will be ignored)
    */
    	String[] parametres;
    	boolean needMore=false;
    	//System.out.println("interprete la ligne : "+line);
    	
    	line=getUsefulLineContent(line);
    	
    	//now, will check what is the element wanted
    	if(line.startsWith("bloc(")) {//make a block. 2 arguments are asked. the arguments asked are position x and position y
    		//System.out.println("faire un bloc");
    		//line=line.replaceFirst("bloc(",  "");//delete the "bloc(" instruction
    		line=line.substring(5,  line.length());//delete the "bloc(" instruction
    		line=line.replace(" ", "");//delete all the spaces (because they are useless)
    		//System.out.println("arguments: "+ line);
			parametres=line.split(";");
        	if (parametres.length==2) {//2 parameters wanted
        		try {//try to turn the arguments into integers
        			int param1, param2;
        			param1=Integer.parseInt(parametres[0]);
        			param2=Integer.parseInt(parametres[1]);
        			handler.addObject(new Block(param1, param2, ID.Enemy, 32, 32));
        			System.out.println("bloc cr��");
        		}
        		catch(Exception probleme) {//if it can't be done, don't do anything
        			System.out.println("arguments invalides");
        		}
    		}
        	else {//not the required number of argument
        		System.out.println("nombre d'arguments invalide");
        	}
    	}
    	else if(line.startsWith("mapSize(")) {
    		line=line.substring(8,  line.length());//delete the "bloc(" instruction
    		line=line.replace(" ", "");//delete all the spaces (because they are useless)

			parametres=line.split(";");
        	if (parametres.length==2) {//2 parameters wanted
        		try {//try to turn the arguments into integers
        			int param1, param2;
        			param1=Integer.parseInt(parametres[0]);
        			param2=Integer.parseInt(parametres[1]);
        			mapSizeX=param1;
        			mapSizeY=param2;
        			System.out.println("taille de map: "+ param1+" "+param2);
        		}
        		catch(Exception probleme) {//if it can't be done, don't do anything
        			System.out.println("arguments invalides");
        		}
    		}
        	else {//not the required number of argument
        		System.out.println("nombre d'arguments invalide");
        	}
    		
    	}
    	else if(line.startsWith("map(")) {
    		System.out.println("creer map");
    		needMore=true;
    	}
    	else {//unknown element
    		System.out.println("ligne ignor�e");
    	}
    	System.out.println("");
    	return needMore;
    }
    
    public void multiLineInterpreter(String lines) {
    	lines=lines.trim();
    	ArrayList<String> line=new ArrayList<String>();
    	for(String l:lines.split("\n")) {
    		//System.out.println(l+"fin");
    		line.add(l);
    	}
    	int len=line.size();
    	if(line.get(0).startsWith("map(")) {//this is the map that is asked to be created
    		System.out.println("creation de la map");
    		line.set(0, line.get(0).substring(7));
    		int height=len;
    		for(int y=0; y<len; y++) {
    			String actualLine=line.get(y).trim();
    			//actualLine.substring(beginIndex)
    			System.out.println("d"+actualLine+"f");
    			int width=actualLine.length();
    			for(int x=0; x<width; x++) {
    				if(actualLine.charAt(x)=='x') {
    					int posx, posy, sizex, sizey;
    					posx=x*(mapSizeX/width);
    					posy=y*(mapSizeY/height);
    					sizex=mapSizeX/width;
    					sizey=mapSizeY/height;
            			handler.addObject(new Block(posx, posy, ID.Enemy, sizex, sizey));
            			System.out.println("bloc: "+posx+" "+posy+" "+sizex+" "+sizey);
    					
    				}
    			}
    		}
    	}
    	
    	
    }
    
    public String getUsefulLineContent(String line) {/**deletes everything that is not useful to the line
    *(things that are after the parenthesis)
    */
    	while(line.length()>0 && line.charAt(line.length()-1)!=')') {//delete everything that is not between the parenthesis (except the element asked)
    		line=line.substring(0,  line.length()-1);
    		//System.out.println("suppr");
    	}
    	if(line.length()>0 && line.charAt(line.length()-1)==')'){//delete the ")" at the end
    		line=line.substring(0,  line.length()-1);
    		//System.out.println("suppr");
    	}
    	
    	return line;
    }
    
    /*public boolean paramNumberOk(int numparam, String line) {//return true if the number of parameter is the one asked (numparam)		USELESS
    	//int param=0;
    	//if (line.split(",").length()==numparam)
    	return (line.split(",").length==numparam);
    }*/
    
    public void mapTransfer(int id, int x, int y) {//lead the player to the map at the position in argument
    	if (id!=this.actualMapID)//do not load the map anew if the wanted map is the actual map
    		loadMap(id);
    	this.actualMapID=id;
    	handler.object.get(0).setX(x);//set the player's position
    	handler.object.get(0).setY(y);
    	camera.setX(handler.object.get(0).getX());
    	camera.setY(handler.object.get(0).getY());
    }
    
    public static int getMapID() {return actualMapID;}
    
   
}
