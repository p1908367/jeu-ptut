package com;

import java.awt.*;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Text extends JFrame{
    public String text;
    public Text(String text){
        super();
        this.text=text;

        build();//On initialise notre fenêtre
    }

    private void build(){
        setTitle(text); //On donne un titre à l'application
        setSize(320,100); //On donne une taille à notre fenêtre
        setLocation(1920/2-160,1080/2+140); //On centre la fenêtre sur l'écran
        setResizable(true); //On permet le redimensionnement
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //On dit à l'application de se fermer lors du clic sur la croix
        setContentPane(buildContentPane());
    }

    private JPanel buildContentPane(){
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        JLabel label = new JLabel(text);


        panel.add(label);

        return panel;
    }


}