package com;

import java.awt.*;

public class Block extends GameObject {
    public Block(int x, int y, ID id, int sX, int sY) {
        super(x, y, id, sX, sY);
        /*w=50;
        h=50;*/
    }

    @Override
    public Rectangle getBounds() {
        return new Rectangle((int )x,(int)y,(int)w,(int)h);
    }

    @Override
    public void tick() {

    }

    @Override
    public void render(Graphics g) {
        Graphics2D g2d=(Graphics2D)g;
        g2d.setColor(Color.red);
        if(id==ID.Tableau)
        	g2d.setColor(Color.yellow);
        else if (id==ID.Talbe)
        	g2d.setColor(Color.blue);
        g2d.draw(getBounds());
        g2d.fillRect(x,y,w,h);

    }
}
