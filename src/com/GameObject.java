package com;

import java.awt.*;

public abstract class GameObject {
    protected int x, y;//position
    protected ID id;
    protected int vitesseX, vitesseY;//speed
    protected int h, w;//size of the object (height and width)
    protected boolean touch;
    protected boolean touch1;
    public boolean u, d, l, r;//boolean that means if the object goes up, down, left or right
    //protected int sizeX, sizeY;

    public abstract Rectangle getBounds();


    public GameObject(int x, int y, ID id, int sX, int sY) {
        this.x = x;
        this.y = y;
        this.id = id;
        this.h=sY;
        this.w=sX;
    }

    public abstract void tick();

    public abstract void render(Graphics g);

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setId(ID id) {
        this.id = id;
    }

    public void setVitesseX(int vitesseX) {
        this.vitesseX = vitesseX;
    }

    public void setVitesseY(int vitesseY) {
        this.vitesseY = vitesseY;
    }

    public void setTouch(boolean touch) {

        this.touch = touch;
    }

    public boolean getTouch() {
        return touch;
    }
    public void setTouch1(boolean touch1) {

        this.touch1 = touch1;
    }

    public boolean getTouch1() {
        return touch1;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public ID getId() {
        return id;
    }

    public int getW() {
        return w;
    }

    public int getH() {
        return h;
    }

    public int getVitesseX() {
        return vitesseX;
    }

    public int getVitesseY() {
        return vitesseY;
    }
    public boolean getU(){
    	return u;
    }
     public boolean getD() {
    	return d;
    }


}
