package com;

import com.sun.nio.file.SensitivityWatchEventModifier;
import javax.swing.JOptionPane;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.Scanner;   //Needed for the Scanner class
import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
import static java.lang.Thread.sleep;

public class Player extends GameObject {
    public Handler handler;
    public String message;
    Console console = System.console();
    int interacts;
    int interacts1;
    static int offsetCameraX;
    static int offsetCameraY;
    private BufferedImage playerSprite;
    private spritsheet ss;
    public int direction=3;
    public int rotationImageDirection=1;
    public int position=0;
    public int frameDelay=0;
    public int frame=1;
    private boolean animateY= false; 
    private boolean animateX= false; 

    public GameObject e;
    

    public Player(int x, int y, ID id, int sX, int sY, Handler handler,Main main) {
        super(x, y, id, sX, sY);
        this.handler = handler;
        ss= new spritsheet (main.getSpriteSheet() );
    	
        playerSprite =ss.getImage(frame,direction,32,32);
        

    }
  
    public void tick() {
        emplacement ();

    	position=x;
    	
    	
    	
    	vitesseY=0;
    	
    	if (u) {//up
    		vitesseY-=2;
    		direction = 4;
    		
        		
        		
    	}
    	if (d) {//down
    		vitesseY+=2;
    		direction =1;
    		
        		
        		}
    	if ( vitesseY !=0 ) {
    		animateY =true;    		
    	}else animateY = false;
    	
    	
    	
    	vitesseX=0;
    	if (l) {//left
    		vitesseX-=2;
    		direction =2;
    		
 
        		}
    	
    	if (r) {//right
    		vitesseX+=2;
    		direction =3;		
        		
    	}
    	if ( vitesseX !=0 ) {
    		animateX =true;    		
    	}else animateX = false;
    	
    	x += vitesseX;
        x = Main.clamp(x, 0, Main.mapSizeX - 45);
        if(Collision())
            text();
        y += vitesseY;
        y = Main.clamp(y, 0, Main.mapSizeY - 68);
        if(Collision())
            text();
        afficherTaquin();
        afficherHowMany();
        
        	
       
        if (vitesseY == 0 && u) {offsetCameraY -= 7;}
        if (vitesseY == 0 && d) {offsetCameraY += 7;}
        
        if (vitesseX == 0 && l) {offsetCameraX -= 7;}
        if (vitesseX == 0 && r) {offsetCameraX += 7;}
        
        if (offsetCameraY >= 0) {offsetCameraY -= 3;};
        if (offsetCameraY <= 0) {offsetCameraY += 3;};
        if (offsetCameraX >= 0) {offsetCameraX -= 3;};
        if (offsetCameraX <= 0) {offsetCameraX += 3;};
        
        if (offsetCameraY >= 200) {offsetCameraY = 200;};
        if (offsetCameraY <= -200) {offsetCameraY = -200;};
        if (offsetCameraX >= 270) {offsetCameraX = 270;};
        if (offsetCameraX <= -270) {offsetCameraX = -270;};
        
        
       if (animateY || animateX ) {
    	   frameDelay++;
           if(frameDelay >= 10) {
        	   frame++;
        	  
           if (frame == 3 ) {
        	   
        	   frame =1;
           }
        	   frameDelay=0;
        	   
           
       }
      }     
       updateSprite();
    }

    private boolean Collision() {

        for (int i = 0; i < handler.object.size(); i++) {
            GameObject tempObject = handler.object.get(i);
            if (tempObject.getId() == ID.Enemy || tempObject.getId() == ID.Talbe || tempObject.getId() == ID.Tableau) {
                if (getBounds().intersects(tempObject.getBounds()))
                    if (vitesseX > 0) {// droite
                        vitesseX = 0;
                        x = tempObject.getX() - 32;
                        return true;
                    } else if (vitesseX < 0) {//gauche
                        vitesseX = 0;
                        x = tempObject.getX() + tempObject.getW();
                        return true;
                    }
            }
            if (tempObject.getId() == ID.Enemy || tempObject.getId() == ID.Talbe|| tempObject.getId() == ID.Tableau) {
                if (getBounds2().intersects(tempObject.getBounds()))
                    if (vitesseY > 0) {// Haut
                        vitesseY = 0;
                        y = tempObject.getY() - 32;
                        return true;
                    } else if (vitesseY < 0) {//Bas
                        vitesseY = 0;
                        y = tempObject.getY() + tempObject.getH();
                        return true;
                    }

            }


        }
        return false;
    }


    private void emplacement () {
    	
    	if (position < x && rotationImageDirection <3)
    		rotationImageDirection +=1;
    	else if ( rotationImageDirection == 3)
        	rotationImageDirection =1;
    }
    private void text (){

        for (int i = 0; i < handler.object.size(); i++) {
            GameObject tempObject = handler.object.get(i);
        if (tempObject.getId() == ID.Talbe) {
            if (getBounds3().intersects(tempObject.getBounds())) {
                    interacts=1; }
                }
        if  ( tempObject.getId()== ID.Tableau){
        	if (getBounds3().intersects(tempObject.getBounds())) {
        		interacts1=1; }
            }
        }
            }
        

     private void afficherTaquin(){
         if (touch == true && interacts==1){
        	   JFrame frame = new JFrame();
               frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
               frame.setTitle("Hack Taquin");
               frame.setResizable(false);
               frame.add(new Taken(3, 500, 30), BorderLayout.CENTER);			//On cr�e une partie
               frame.pack();
               // center on the screen
               frame.setLocationRelativeTo(null);
               frame.setVisible(true);
               touch = false;
               interacts=0;
         }
     }
     private void afficherHowMany(){
         if (touch1 == true && interacts1==1){
        	HowMany howMany= new HowMany();
        	
             touch1 = false;
             interacts1=0;
          
         }
     }


    public Rectangle getBounds3() { //pour les interaction avec l'object
        float bx=x +vitesseX+1;
        float by=y;
        float bw=w+1+vitesseX/2;
        float bh=h;

        return new Rectangle((int)bx,(int)by,(int)bw,(int)bh);
    }
    public Rectangle getBounds4(){ //pour les interaction avec l'object
        float bx=x;
        float by=y+vitesseY+1;
        float bw=w;
        float bh=h+vitesseY/2+1;
        return new Rectangle((int)bx,(int)by,(int)bw,(int)bh);

    }

    public Rectangle getBounds() {  //pour les collision avec l'object
       float bx=x +vitesseX;
       float by=y;
       float bw=w+vitesseX/2;
       float bh=h;
       return new Rectangle((int)bx,(int)by,(int)bw,(int)bh);
    }
    public Rectangle getBounds2(){  //pour les collision avec l'object
        float bx=x;
        float by=y+vitesseY;
        float bw=w;
        float bh=h+vitesseY/2;
        return new Rectangle((int)bx,(int)by,(int)bw,(int)bh);

    }
    
    private void updateSprite(){
    	playerSprite =ss.getImage(frame,direction,32,32);
    }
    
    public void render(Graphics g) {
    	
         
    	g.drawImage(playerSprite,(int)x,(int)y,null);
        

    }
    
    public static int getOffsetCameraX() {
    	return offsetCameraX;
    }
    
    public static int getOffsetCameraY() {
    	return offsetCameraY;
    }



}
