package com;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import com.Main.STATE;

public class KeyInput extends KeyAdapter {
    private Main game;

    private Handler handler;
    public KeyInput(Handler handler, Main game) {
        this.handler = handler;
        this.game=game;

    }

    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();
        if(game.gameState==Main.STATE.game) {
        for (int i = 0; i < handler.object.size(); i++) {
            GameObject tempObject = handler.object.get(i);
            if (tempObject.getId() == ID.Player) {
                // C'est le déplacement du Joueur 1
                /*if (key == KeyEvent.VK_UP) tempObject.setVitesseY(-5);
                if (key == KeyEvent.VK_DOWN) tempObject.setVitesseY (+5);
                if (key == KeyEvent.VK_RIGHT) tempObject.setVitesseX( +5);
                if (key == KeyEvent.VK_LEFT) tempObject.setVitesseX(-5);*/
                if (key == KeyEvent.VK_UP) tempObject.u=true;
                if (key == KeyEvent.VK_DOWN) tempObject.d=true;
                if (key == KeyEvent.VK_RIGHT) tempObject.r=true;
                if (key == KeyEvent.VK_LEFT) tempObject.l=true;
                if (key == KeyEvent.VK_X) tempObject.setTouch(true);
                if (key == KeyEvent.VK_N) tempObject.setTouch1(true);
                if (key == KeyEvent.VK_ESCAPE) {StaticMethod sm=new StaticMethod(handler, game);game.gameState= Main.STATE.menu;}
                if (key == KeyEvent.VK_U) {Main.experience += 10;}
                if (key == KeyEvent.VK_S && tempObject.u == false && tempObject.d == false && tempObject.r == false && tempObject.l == false) {
                	Save.saveGame();
                	}
                if (key == KeyEvent.VK_C) {if (Main.camState==false) {Main.camState=true;} else Main.camState=false;}
                //if (key == KeyEvent.VK_ESCAPE) {game.gameState= Main.STATE.menu;} 	
            }     
            
        
        if (key == KeyEvent.VK_SPACE && tempObject.getX()==0 && tempObject.getY()==0) {//to test the map transfer in game, it will switch between the two maps
        	int id=(handler.game.getMapID())%2+1;
        	handler.game.mapTransfer(id, 50, 50);
        	System.out.println("map:"+id);
        	}
        }
    	}
        
    }



    public void keyReleased(KeyEvent e) {
        int key = e.getKeyCode();
        for (int i = 0; i < handler.object.size(); i++) {
            GameObject tempObject = handler.object.get(i);
            if (tempObject.getId() == ID.Player) {
                // C'est le déplacement du Joueur 1
                /*if (key == KeyEvent.VK_UP) tempObject.setVitesseY(0);
                if (key == KeyEvent.VK_DOWN) tempObject.setVitesseY (0);
                if (key == KeyEvent.VK_RIGHT) tempObject.setVitesseX( 0);
                if (key == KeyEvent.VK_LEFT) tempObject.setVitesseX(0);*/
                if (key == KeyEvent.VK_UP) tempObject.u=false;
                if (key == KeyEvent.VK_DOWN) tempObject.d=false;
                if (key == KeyEvent.VK_RIGHT) tempObject.r=false;
                if (key == KeyEvent.VK_LEFT) tempObject.l=false;
                if (key == KeyEvent.VK_X ) tempObject.setTouch(false);
                if (key == KeyEvent.VK_N) tempObject.setTouch1(false);

            }


            }
        }

    }



