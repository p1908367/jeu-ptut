package com;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Save extends GameObject{
	
	//private Object playerPos = StaticMethod.getNonStaticVariable();

	public Save(int x, int y, ID id) {
		super(x, y, id, 0, 0);
		// TODO Auto-generated constructor stub
	}
	
	private static boolean erreur=false;
	private Handler handler;
	
	/*public Save(int x, int y, ID id) {
    	try {
  	      File Save = new File("Save/Save.txt");
  	      if (Save.createNewFile()) {
  	        System.out.println("Fichier de sauvegarde cr�e : " + Save.getName());
  	      } else {
  	        System.out.println("Fichier de sauvegarde d�j� existant.");
  	      }
  	      FileWriter saveUpdate = new FileWriter("Save.txt");
  	      saveUpdate.write("Player coord : " + Integer.toString(handler.object.get(0).getX()));
  	      saveUpdate.close();
  	      System.out.println("Partie sauvegard�e");
  	    } catch (IOException e) {
  	      System.out.println("Erreur");
  	      e.printStackTrace();
  	    }  
	}*/
	
	public static void saveGame(){
	    try {
	      File Save = new File("files/Save/Save1.txt");
	      if (Save.createNewFile()) {
	        System.out.println("Fichier de sauvegarde cr�e : " + Save.getName());
	      } else {
	        System.out.println("Fichier de sauvegarde d�j� existant.");
	      }
	      
	      FileWriter saveUpdate = new FileWriter("files/Save/Save1.txt");
	      saveUpdate.write("PlayerXY : "+StaticMethod.posX+"; "+StaticMethod.posY+"\n");
	      saveUpdate.write("Player$ : "+Main.argent+"\n");  //Save player Money
	      saveUpdate.write("PlayerXP : "+Main.experience+ "\nPlayerLVL : "+Main.niveau+"\n");  //Save player XP and level
	      saveUpdate.write("PlayerMap : "+StaticMethod.playerMap+"\n");  //Saves the map where the player is on
	      
	      
	      
	      saveUpdate.close();
	      System.out.println("Partie sauvegard�e");
	    } catch (IOException e) {
	      System.out.println("Erreur");
	      e.printStackTrace();
	    }    
	    
	  }
	
	public static void loadGame() {
		try {
		      File Save = new File("files/Save/Save1.txt");
		      if (Save.exists()) {
		        System.out.println("Partie charg�e : " + Save.getName());
		      } else {
		        System.out.println("Fichier de sauvegarde inexistant.");
		      }
		      
		      FileWriter saveUpdate = new FileWriter("files/Save/Save1.txt");
		      saveUpdate.write("Player x , Player y");
		      saveUpdate.close();
		      System.out.println("Partie sauvegard�e");
		    } catch (IOException e) {
		      System.out.println("Erreur");
		      e.printStackTrace();
		    }   
		
	}
	
	
	public static void loadSave() /*throws Exception*/ {
		//handler=new Handler(this);
        File save = new File("files/Save/Save"+"1"+".txt");
        //File save = new File("save1.txt");
    	//System.out.println(arguments[0]+"save1");
		//File save = new File(arguments[0]+"save1");
        //File save = new File(arguments[0]+"save"+id+".txt");
        String saveInfo="";
        if (save.isFile() && save.canRead() ) {
        	System.out.println("la save est utilisable");
        	try {
        	FileReader rd = new FileReader(save);
    		int ch;
    		while((ch=rd.read())!=-1) {
    			saveInfo+=(char)ch;
    		}
    		rd.close();
    		saveInfo=saveInfo.trim();//deletes the useless spaces at the beginning and the end of the string
    		//System.out.println(saveInfo);
        	}
        	catch(Exception probleme) {
        		System.out.println("Une erreur est survenue lors du chargement de la save");
        		erreur=true;
        	}
        }
        else {
        	System.out.println("erreur, la save n'est pas utilisable ou est introuvable");
        	erreur=true;
        }
        
    	//if (id==1) {
        //handler.addObject(new Player(100, 100, ID.Player, handler));
    	/*if (id==1) {
            handler.addObject(new Block(Main.WIDTH / 2, Main.HEIGHT / 2, ID.Enemy));
            handler.addObject(new Block(100, 55, ID.Enemy));
            handler.addObject(new Block(300, 160, ID.Talbe));
    	}
    	else {
            handler.addObject(new Block(200, 200, ID.Talbe));
    	}*/
        createsave(saveInfo);
    }
    
    public static void createsave(String saveinfos) {
    	System.out.println("infos de la save:\n"+saveinfos+"\n\n");
    	String actualLine="";//will contain each line of the save (each line of the file)
    	int actualChar=0, /*lineStart=0,*/ len=saveinfos.length();//contains the index of the start of the line and the actual character
    	while (actualChar<len) {
    		if(saveinfos.charAt(actualChar)=='\n') {//end of line
    			System.out.println(actualLine);
    			lineInterpreter(actualLine);
    			actualLine="";
    		}
    		else if(actualChar==len-1) {
    			actualLine+=saveinfos.charAt(actualChar);
    			System.out.println(actualLine);
    			lineInterpreter(actualLine);
    			actualLine="";
    		}
    		else
    			actualLine+=saveinfos.charAt(actualChar);
    		actualChar+=1;
    	}
    	
    }
    
    public static void lineInterpreter(String line) {/**interprets the line, creating an element of the save.
    * if the line is misspelled or unknown, does nothing
    * 
    * Rules: -example: ElementName(parameter1; parameter 2, ...)
    * 	-parameters are split by a ';'
    * 	-spaces in parameters that are not strings are ignored
    * 	-a line MUST be finished by a ')' to work properly! the line wont be used otherwise. (everything after the ')' will be ignored)
    */
    	String[] parametres;
    	//System.out.println("interprete la ligne : "+line);
    	while(line.length()>0 && line.charAt(line.length()-1)!=')') {//delete everything that is not between the parenthesis (except the element asked)
    		line=line.substring(0,  line.length()-1);
    		//System.out.println("suppr");
    	}
    	if(line.length()>0 && line.charAt(line.length()-1)==')'){//delete the ")" at the end
    		line=line.substring(0,  line.length()-1);
    		//System.out.println("suppr");
    	}
    	
    	//now, will check what is the element wanted
    	if(line.startsWith("PlayerXY :")) {//make a block. 2 arguments are asked. the arguments asked are position x and position y
    		//System.out.println("faire un bloc");
    		//line=line.replaceFirst("bloc(",  "");//delete the "bloc(" instruction
    		line=line.substring(5,  line.length());//delete the "bloc(" instruction
    		line=line.replace(" ", "");//delete all the spaces (because they are useless)
    		//System.out.println("arguments: "+ line);
			parametres=line.split(";");
        	if (parametres.length==2) {//2 parameters wanted
        		try {//try to turn the arguments into integers
        			int param1, param2;
        			param1=Integer.parseInt(parametres[0]);
        			param2=Integer.parseInt(parametres[1]);
        			System.out.println("Partie interpr�t� "+param1+" "+param2);
        			//handler.object.get(0).setX(param1);
        			//handler.object.get(0).setY(param2);
        			//handler.addObject(new Block(param1, param2, ID.Enemy, 32, 32));
        			System.out.println("Joueur positionn�");
        		}
        		catch(Exception probleme) {//if it can't be done, don't do anything
        			System.out.println("arguments invalides");
        		}
    		}
        	else {//not the required number of argument
        		System.out.println("nombre d'arguments invalide");
        	}
    	}
    	else {//unknown element
    		System.out.println("ligne ignor�e");
    	}
    	System.out.println("");
    }
    
    /*public boolean paramNumberOk(int numparam, String line) {//return true if the number of parameter is the one asked (numparam)		USELESS
    	//int param=0;
    	//if (line.split(",").length()==numparam)
    	return (line.split(",").length==numparam);
    }*/
    
    /*public void saveTransfer(int id, int x, int y) {//lead the player to the save at the position in argument
    	if (id!=this.actualsaveID)//do not load the save anew if the wanted save is the actual save
    		loadSave(id);
    	this.actualsaveID=id;
    	handler.object.get(0).setX(x);//set the player's position
    	handler.object.get(0).setY(y);
    	camera.setX(handler.object.get(0).getX());
    	camera.setY(handler.object.get(0).getY());
    }*/
	
	
	
	
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Override
	public Rectangle getBounds() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void tick() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(Graphics g) {
		// TODO Auto-generated method stub
		
	}

}