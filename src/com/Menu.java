package com;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import com.Main.STATE;

public class Menu extends MouseAdapter {
        private Main game;
        private Handler handler;

        public Menu(Main game,Handler handler){
            this.game=game;
            this.handler=handler;
        }

    public void mousePressed(MouseEvent e){
    	if (game.gameState== STATE.menu) {//because if not checked, we can launch again the game or exit by clicking on the hidden buttons
            int mx=e.getX();
            int my=e.getY();
            if (mouseOver(mx,my,220,100,200,64)){//play button
            	game.gameState= Main.STATE.game;
                /*handler.addObject(new Player(100, 100, ID.Player, handler));
                handler.addObject(new Block(Main.WIDTH / 2, Main.HEIGHT / 2, ID.Enemy));
                handler.addObject(new Block(100, 55, ID.Enemy));
                handler.addObject(new Block(300, 160, ID.Talbe));*/
                if (game.launched==false) {
                	game.mapTransfer(1, 100, 200);
                	game.launched=true;
                }


            }
            if (mouseOver(mx,my,110,200,200,64)) {//Save button
            	Save.saveGame();
            }
            if (mouseOver(mx,my,340,200,200,64)) {//Load button
            	Save.loadSave();
            	
            }
            if (mouseOver(mx,my,220,300,200,64)) {//exit button
            	System.exit(1);
            }
    	}
       }

    public void MouseReleased(MouseEvent e){

    }
    private boolean mouseOver(int mx,int my,int x,int y, int width,int height){
        if(mx >x && mx<x+width){
            if (my>y && my<y+height){
                return true;
            }else return false;
        }else return false;
    }
    public void render(Graphics g){
            Font fnt = new Font( "arial ",1,50);
            Font fnt2 = new Font( "arial ",1,30);
            int x=game.camera.getX()*2;
            int y=game.camera.getY()*2;
            
            Graphics2D g2d = (Graphics2D) g.create();
            float alpha =  0.3f;
            AlphaComposite alcom = AlphaComposite.getInstance(
                    AlphaComposite.SRC_OVER, alpha);
            
            g2d.setComposite(alcom);
            g2d.setColor(Color.black);
            g2d.fillRect(x, y, game.WIDTH, game.HEIGHT);
            
            g.setFont(fnt);
            g.setColor(Color.red);
            g.drawString("Menu",x+240,y+75);
            
            
            g.setColor(Color.white);
            g.fillRect(x+220,y+100,200,64);
            g.setColor(Color.black);
            g.setFont(fnt2);
            g.drawRect(x+220,y+100,200,64);
            g.drawString("Play",x+280,y+140);

            g.setColor(Color.white);
            g.fillRect(x+110,y+200,200,64);
            g.setColor(Color.black);
            g.drawRect(x+110,y+200,200,64);
            g.drawString("Save",x+170,y+240);
            
            g.setColor(Color.white);
            g.fillRect(x+340,y+200,200,64);
            g.setColor(Color.black);
            g.drawRect(x+340,y+200,200,64);
            g.drawString("Load",x+400,y+240);

            g.setColor(Color.white);
            g.fillRect(x+220,y+300,200,64);
            g.setColor(Color.black);
            g.drawRect(x+220,y+300,200,64);
            g.drawString("Exit",x+280,y+340);
    }
    public void tick(){

    }

}
