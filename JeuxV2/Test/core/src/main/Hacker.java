package main;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.mygdx.game.screen.GameScreen;

public class Hacker extends com.badlogic.gdx.Game {
    private GameScreen screen;
    @Override
    public void create() {
        screen= new GameScreen(this);
        this.setScreen(screen);
    }

    @Override
    public void render(){
        Gdx.gl.glClearColor(0f,0f,0f,1f);  //c'est pour effacer l'ecran entre 2 ecran
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);               //c'est pour effacer l'ecran entre 2 ecran
        super.render();
    }

}
