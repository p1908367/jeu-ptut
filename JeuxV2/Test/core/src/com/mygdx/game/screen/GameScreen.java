package com.mygdx.game.screen;

import Controller.PlayerController;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import main.Hacker;
import main.Settings;
import model.*;

public class GameScreen extends AbstractScreen {
    private Actor player;
    private PlayerController controller;
    private TileMap map;
    private Camera camera;
    private SpriteBatch batch;
    private Texture StandingSouth   ;
    private Texture sol1;
    private Texture sol2;
    public GameScreen(Hacker app) {
        super(app);
        StandingSouth = new Texture("brendan_bike_east_0.png");
        sol1 = new Texture("indoor_tiles.png");
        sol2 = new Texture("indoor_tiles_shadow.png");


        batch = new SpriteBatch();

        map= new TileMap(10,10);
        player = new Actor(map,0,0);
        controller=new PlayerController(player);
        camera= new Camera();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(controller);
    }

    @Override
    public void render(float delta) {
        player.update(delta);
        camera.update(player.getWorldX()+0.5f,player.getWorldY()+0.5f);

        batch.begin();
    float worldStartX=Gdx.graphics.getWidth()/2-camera.getCameraX()*Settings.SCALED_TILE_SIZE;
    float worldStartY=Gdx.graphics.getHeight()/2-camera.getCameraY()*Settings.SCALED_TILE_SIZE;

        for (int x=0; x< map.getWidth() ;x++){
        for (int y=0; y< map.getHeight();y++ ){
            Texture render;
            if (map.getTile(x,y).getTerrain() == TERRAIN.GRASS_1){
                render=sol1;
            }else {
                render=sol2;
            }

            batch.draw(render,
                    worldStartX+x*Settings.SCALED_TILE_SIZE,worldStartY+y*Settings.SCALED_TILE_SIZE,Settings.SCALED_TILE_SIZE,Settings.SCALED_TILE_SIZE);
        }
        }

    batch.draw(StandingSouth, worldStartX+player.getWorldX()* Settings.SCALED_TILE_SIZE, worldStartY+player.getWorldY()*Settings.SCALED_TILE_SIZE,Settings.SCALED_TILE_SIZE,Settings.SCALED_TILE_SIZE*1.5f);
    batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void update(float delta) {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

}
