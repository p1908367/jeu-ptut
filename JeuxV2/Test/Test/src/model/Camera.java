package model;

public class Camera {
    private float cameraX = 0f;
    private float CameraY = 0f;

    public void update(float newCamX, float newCamY) {
        this.cameraX = newCamX;
        this.CameraY = newCamY;
    }

    public float getCameraX() {
        return cameraX;
    }

    public float getCameraY() {
        return CameraY;
    }
}
